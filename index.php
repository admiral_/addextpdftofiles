<?php
$path = './src';
$ext = array('php', 'html', 'js');
function searchDirectory($path)
{
    $dir = '';
    foreach (glob($path . '/*') as $file) {
        if(is_dir($file)){
            $dir = basename($file);
        }
    }
    return $dir;
}
function searchFiles($path, $files)
{
    $searchFile = array();
    $files = implode(',', $files);
    foreach (glob($path . '/*.{' . $files .'}', GLOB_BRACE) as $file) {
        if(is_file($file)){
            $searchFile[] = $path . '/' . basename($file);
        }
    }
    return $searchFile;
}

function addPdfExtToFiles($paths)
{

    foreach ($paths as $key => $path) {
        if(!copy($path, $path . '.pdf')){
            echo "can't copy $path..." . PHP_EOL;
        } else {
            echo "Создан новый .pdf файл по пути $path.pdf" . PHP_EOL;
        }
    }
}

echo 'Директория: "'.$path.'"' . PHP_EOL;
$first = searchDirectory($path);
$second = searchDirectory($path . '/' . $first);
$all_dir = $path . '/' . $first . '/' . $second;
$files = searchFiles($all_dir, $ext);

foreach ($files as $key => $value) {

    echo 'Найден файл по пути "' . $value . '"' . PHP_EOL;
}
addPdfExtToFiles($files);


